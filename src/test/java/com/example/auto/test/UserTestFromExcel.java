package com.example.auto.test;

import com.example.auto.BaseTest;
import com.example.auto.utils.HttpClientUtil;
import com.example.auto.utils.ReadFromExcel;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

/**
 * 读取外部文件测试用例数据源
 */
@Slf4j
public class UserTestFromExcel extends BaseTest {

    private String fileName = "autoInterfaceTest.xlsx";

    @Autowired
    private ObjectMapper objectMapper;

    @DataProvider(name = "userData")
    public Object[] getUserData() {
        return ReadFromExcel.testData(fileName);
    }

    @Test(dataProvider = "userData", suiteName = "suiteName", testName = "testName")
    public void commonTest(HashMap<String, String> data) throws IOException {

        log.info("requestData============{}", data);
        String expectName = data.get("expectName");
        String compare = data.get("compare");
        String expectValue = data.get("expectValue");

        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(data.get("url"));
        httpPost.setEntity(new StringEntity(data.get("requestJson")));
        httpPost.setHeader("Content-Type", "application/json;charset=utf8");
        CloseableHttpResponse response = httpClient.execute(httpPost);
        String result = EntityUtils.toString(response.getEntity());
        log.info("result===================={}", result);

        JsonNode jsonNode = objectMapper.readTree(result);

        String responseValue = jsonNode.get(expectName).asText();
        if (Objects.equals(compare, "equal")) {
            Assert.assertEquals(responseValue, expectValue);
        } else if (Objects.equals(compare, "noEqual")) {
            Assert.assertNotEquals(response, expectValue);
        } else {
            System.out.println("compare======" + compare);
        }
    }

    @Test()
    public void test2() throws IOException {

        String url = "http://localhost:9002/user/update/by/token";

        HashMap<String, String> requestMap = new HashMap<>();
        requestMap.put("name", "shao");
        requestMap.put("phone", "demoData");

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put("token", "50785b71-8a58-45af-aebd-616804b28788");

        String result1 = HttpClientUtil.doPost(url, requestMap, headerMap);
        log.info("result1===================={}", result1);

        String requestJson = objectMapper.writeValueAsString(requestMap);
        log.info("requestJson={}", requestJson);
        String result = HttpClientUtil.doPost(url, requestJson, headerMap);
        log.info("result===================={}", result);
    }

}
