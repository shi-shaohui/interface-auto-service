package com.example.auto.test;

import com.example.auto.BaseTest;
import org.springframework.stereotype.Service;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;

/**
 * 本地模拟数据源测试
 */
@Service
public class UserTestSampler extends BaseTest {

    @DataProvider(name = "userData")
    public Object[] getUserData() {

        HashMap<String, String>[] data = new HashMap[2];
        HashMap<String, String> map1 = new HashMap<>();
        map1.put("name", "shi");
        map1.put("sex", "man");
        HashMap<String, String> map2 = new HashMap<>();
        map2.put("name", "shao");
        map2.put("sex", "women");

        data[0] = map1;
        data[1] = map2;

        return data;
    }

    @Test(dataProvider = "userData",testName = "123",suiteName = "suiteName")
    public void commonTest(HashMap<String,String> data) {
        System.out.println(data.toString());
        Assert.assertEquals(data.get("name"),"shi");
    }
}
