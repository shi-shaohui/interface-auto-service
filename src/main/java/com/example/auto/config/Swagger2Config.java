package com.example.auto.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shishaohui
 */
@EnableSwagger2
@Configuration
public class Swagger2Config {

    @Bean
    public Docket getUserDocket(){
        ApiInfo apiInfo=new ApiInfoBuilder()
                .title("du01")//api标题
                .description("测试练习服务")//api描述
                .version("1.0.0")//版本号
                .contact(new Contact("shi","http://shishaohui","18513551468@163.com"))//本API负责人的联系信息
                .build();
        return new Docket(DocumentationType.SWAGGER_2)//文档类型（swagger2）
                .apiInfo(apiInfo)//设置包含在json ResourceListing响应中的api元信息
                .select()//启动用于api选择的构建器
                .apis(RequestHandlerSelectors.basePackage("com.example.du01.controller"))//扫描接口的包
                .paths(PathSelectors.any())//路径过滤器（扫描所有路径）
                .build()
                .globalOperationParameters(getParameterList());
    }

    private List<Parameter> getParameterList() {
        List<Parameter> parameterList = new ArrayList<>();
        ParameterBuilder tokenParam = new ParameterBuilder();
        tokenParam.name("token").description("登录token")
                .modelRef(new ModelRef("string"))
                .parameterType("header").required(false).build();

        parameterList.add(tokenParam.build());
        return parameterList;
    }
}
