package com.example.auto.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Shishaohui
 */
@Slf4j
@RestController
@RequestMapping("/heath")
@Api(tags = "健康检查")
public class HeathController {

    @GetMapping("/check")
    public String check() {
        log.info("check start");
        return "heath check success!";
    }


}
